/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package titleII;

/**
 *
 * @author S530742
 */
public enum Major {

    AgricultureEducation(7967),
    AgriculturalEducation(7967),
    Art(7941),
    Biology(7942),
    BusinessEducation(7943),
    Chemistry(7946),
    EarthScience(7938),
    ElementaryEducation(7647),
    English(7948),
    EnglishEducation(7948),
    GeneralScience(7950),
    Health(7953),
    IndustrialTechnology(7954),
    Journalism(7956),
    Marketing(7904),
    Mathematics(7958),
    PhysicalEducation(7960),
    Physics(7959),
    SocialScience(7966),
    SpeechandTheatre(7965),
    UnifiedScienceBiology(79742),
    UnifiedScienceChemistry(79746),
    UnifiedScienceEarthScience(79738),
    UnifiedSciencePhysics(79759),
    SpecialEducCrossCategorical(5101),
    InstruMusicEducNonPiano(555),
    //-------------------------------------------------------------------------
    EdSpecialistPrincipalElem(0),
    SpecialEducation(5101),
    MiddleSchool(0),
    EducationalLeadershipElem(0),
    Reading(0),
    MUDoctoralProgram(0),
    EdSpecialistSuperintendent(0),
    TeachingSecAgriculture(0),
    MathematicsEducation(7958),
    ChemistryEducation(7946),
    EdLeadershipK12(0),
    VocalMusicEducVoice(569),
    GuidanceandCounseling(0),
    SpeechTheatreEducation(7965),
    TeachingEarlyChildhood(0),
    TeachingElemSelfContained(0),
    TchgInstructionalTechnology(0),
    EducationalLeadershipSec(0),
    Spanish(563),
    EdSpecialistPrincipalSec(0),
    TeachingEnglishLangLrnrs(0),
    TeachingScience(0),
    ArtEducation(0),
    BiologyEducation(7942),
    HealthandPhysicalEducation(0),
    NonDegree(0),
    TeachingMathematics(0),
    AlternativeCertification(999),
    TeachingHistory(0);
    private final int majorCode;

    Major(int majorCode) {
        this.majorCode = majorCode;
    }

    /**
     * this is a getter method to get a value
     *
     * @return double type
     */
    public int getMajorCode() {
        return majorCode;
    }

}
