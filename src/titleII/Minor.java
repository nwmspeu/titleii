/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package titleII;

/**
 *
 * @author Tk20
 */
public enum Minor {
    
    MathEducation (7758),
    MathEducation59 (7758),
    EarlyChildhoodEducation (1125),
    EarlyChildhdSpecialEdCert (119),
    MidSchConLangArts21hr (7719),
    MidSchCertSocialScience(0),
    DeafStudies (572),
    ElementaryArtEducation (0),
    PhysicsEducation(460),
    ChildandFamilyStudies(0),
    MiddleSchoolScience(0),
    Spanish(563),
    MidSchEndrsSpeechTheatre (7765),
    HealthEducation (553),
    Communication (0),
    Psychology (0),
    MidSchEndorsementBusEd (0),
    English912 (7948),
    English(541),
    GeneralBusiness (7743),
    SportManagement (0),
    Coaching (0),
    Music (555),
    Journalism (7956),
    History (0),
    InternationalStudies (0),
    EnglishWriting(0),
    ComputerScience (0),
    Horticulture (0),
    IndividualizedLanguage (0),
    Geology (0),
    SportPsychology (0),
    Art (541),
    AnimalScience(0),
    BusinessEducation(7743),
    MarketingCooperativeEduc (0);
    
    
    private final int minorCode;
    
    Minor(int minorCode)
    {
        this.minorCode = minorCode;
    }

   
    public int getMinorCode() {
        return minorCode;
    }
    
    
    
}
