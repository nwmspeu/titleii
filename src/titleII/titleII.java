/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package titleII;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Naveen Kumar chandaluri
 */
public class titleII {

    public static void main(String[] args) throws FileNotFoundException, IOException, InvalidFormatException {

        String lname = null, fname = null, admit = null, major1 = null, major2 = null, major3 = null, major4 = null;
        int code1, code2, code3, code4;
        String progType = null;
        String repType = null;
        String uniqueID = null;

        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        int returnValue = jfc.showOpenDialog(null);
        File selectedFile = null;
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            selectedFile = jfc.getSelectedFile();
        }

        FileInputStream file = new FileInputStream(selectedFile);
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        FileInputStream myxls = new FileInputStream("output.xlsx");
        XSSFWorkbook workbook2 = new XSSFWorkbook(myxls);
        XSSFSheet sheet2 = workbook2.getSheetAt(0);

        XSSFSheet sheet = workbook.getSheetAt(0);
        Row row = sheet.getRow(2);

        DataFormatter formatter = new DataFormatter();
        int l = 10;
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            ArrayList<Integer> codeList = new ArrayList<Integer>();

            String minor1 = null, minor2 = null, minor3 = null, minor4 = null;
            int m1 = 0, m2 = 0, m3 = 0, m4 = 0, m5 = 0, m6 = 0, m7 = 0, m8 = 0;

            if (formatter.formatCellValue(sheet.getRow(i).getCell(19)).equals("Y")) {
                l++;
                lname = formatter.formatCellValue(sheet.getRow(i).getCell(2));
                fname = formatter.formatCellValue(sheet.getRow(i).getCell(3));
                admit = formatter.formatCellValue(sheet.getRow(i).getCell(19));
                repType = formatter.formatCellValue(sheet.getRow(i).getCell(9)).replaceAll(" ", "");
                progType = formatter.formatCellValue(sheet.getRow(i).getCell(22)).replace(".", "").replace(":", "");
                uniqueID = formatter.formatCellValue(sheet.getRow(i).getCell(1));

                if (progType.equals("Alternative Certification")) {
                    progType = "A";
                } else {
                    progType = "T";
                }

                if (repType.equalsIgnoreCase("y")) {
                    repType = "3";
                } else {
                    repType = "2";
                }

                major1 = formatter.formatCellValue(sheet.getRow(i).getCell(22)).replace(".", "").replace(":", "");
                if (!major1.isEmpty()) {
                    m1 = Major.valueOf(major1.replace("/", "").replace("-", "").replaceAll(" ", "")).getMajorCode();
                }
                major2 = formatter.formatCellValue(sheet.getRow(i).getCell(23)).replace(".", "").replace(":", "");
                if (!major2.isEmpty()) {
                    m2 = Major.valueOf(major2.replace("/", "").replace("-", "").replaceAll(" ", "")).getMajorCode();
                }
                major3 = formatter.formatCellValue(sheet.getRow(i).getCell(24)).replace(".", "").replace(":", "");
                if (!major3.isEmpty()) {
                    m3 = Major.valueOf(major3.replace("/", "").replace("-", "").replaceAll(" ", "")).getMajorCode();
                }
                major4 = formatter.formatCellValue(sheet.getRow(i).getCell(25)).replace(".", "").replace(":", "");
                if (!major4.isEmpty()) {
                    m4 = Major.valueOf(major4.replace("/", "").replace("-", "").replaceAll(" ", "")).getMajorCode();
                }

                minor1 = formatter.formatCellValue(sheet.getRow(i).getCell(26)).replace(".", "").replace(":", "");
                if (!minor1.isEmpty()) {
                    m5 = Minor.valueOf(minor1.replace("/", "").replace("(", "").replace(")", "").replace("-", "").replaceAll(" ", "")).getMinorCode();
                }

                minor2 = formatter.formatCellValue(sheet.getRow(i).getCell(27)).replace(".", "").replace(":", "");
                if (!minor2.isEmpty()) {
                    m6 = Minor.valueOf(minor2.replace("/", "").replace("-", "").replace("(", "").replace(")", "").replaceAll(" ", "")).getMinorCode();
                }

                minor3 = formatter.formatCellValue(sheet.getRow(i).getCell(28)).replace(".", "").replace(":", "");
                if (!minor3.isEmpty()) {
                    m7 = Minor.valueOf(minor3.replace("/", "").replace("-", "").replace("(", "").replace(")", "").replaceAll(" ", "")).getMinorCode();
                }

                minor4 = formatter.formatCellValue(sheet.getRow(i).getCell(29)).replace(".", "").replace(":", "");
                if (!minor4.isEmpty()) {
                    m8 = Minor.valueOf(minor4.replace("/", "").replace("-", "").replace("(", "").replace(")", "").replaceAll(" ", "")).getMinorCode();
                }

                if (m1 > 0) {
                    codeList.add(m1);
                }

                if (m2 > 0) {
                    codeList.add(m2);
                }

                if (m3 > 0) {
                    codeList.add(m3);
                }

                if (m4 > 0) {
                    codeList.add(m4);
                }

                if (m5 > 0) {
                    codeList.add(m5);
                }

                if (m6 > 0) {
                    codeList.add(m6);
                }

                if (m7 > 0) {
                    codeList.add(m7);
                }

                if (m8 > 0) {
                    codeList.add(m8);
                }
                
                // System.out.println(uniqueID);

                //        System.out.print(lname+" "+fname+" "+ progType+" "+repType+ " " );
                sheet2.createRow(l).createCell(2).setCellValue(lname);
                sheet2.getRow(l).createCell(0).setCellValue(uniqueID);
                sheet2.getRow(l).createCell(3).setCellValue(fname);
                sheet2.getRow(l).createCell(6).setCellValue(progType);
                sheet2.getRow(l).createCell(7).setCellValue(repType);
                

                int k = 8;
                for (Integer n : codeList) {
                    //          System.out.print(n+" ");
                    sheet2.getRow(l).createCell(k).setCellValue(n);
                    k++;

                }
                //    System.out.println();           
            }
        }
        FileOutputStream out = new FileOutputStream(new File("output.xlsx"));
        workbook2.write(out);
        out.close();
        file.close();
    }
}
