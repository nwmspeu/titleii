********************************************
	     Title II
******************************************** 

Input : EDUC_MAJ_MCBRIDE.xlsx
Output: output.xlsx

Generating Program type is 
        A for Alternative Certification
	T for other courses 
based on the MAJR1 column.

Generating codes for the courses to all the students for Major1, Major2, Major3, Major4, Minor1, Minor2, Minor3 and Minor4.
We are hard coded all the codes for the courses in the enums Major.java for major courses, Minor.java for minor courses.

Note: If any code is updated to the courses, then update the code details in the Major.java and Minor.java before running.

ERROR:

If you see any error while running the code, may be course is not exists in the Major.java and Minor.java. So, please add
the coursename and course code details.
Add course details separatly for major and minor.

